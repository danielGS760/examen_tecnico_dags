package com.trupper.examen_tecnico.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.trupper.examen_tecnico.model.Producto;

@Repository
public interface IProductosRepository extends CrudRepository<Producto, Integer> {

}
