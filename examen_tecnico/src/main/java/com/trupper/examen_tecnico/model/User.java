package com.trupper.examen_tecnico.model;

import java.io.Serializable;

public class User implements Serializable {

	private static final long serialVersionUID = 7973456493134805976L;

	private String id;
	private String token;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
