package com.trupper.examen_tecnico.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.trupper.examen_tecnico.model.User;

@RestController
@RequestMapping(path = "/users")
public interface IUserController {
	
	@PostMapping("/login")
	public User autheticate(@RequestParam("usuario") String usuario, @RequestParam("pass") String password);

}
