package com.trupper.examen_tecnico.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.trupper.examen_tecnico.model.Producto;
import com.trupper.examen_tecnico.service.IProductosService;

@RestController
public class ProductosController implements IProductosController {

	@Autowired
	private IProductosService service;

	@Override
	public Producto actualizarProducto(Producto producto, int id) {
		return service.actualizarPrecioProducto(producto.getPrecio(), id);
	}

	@Override
	public Producto obtenerProducto(int id) {
		return service.obtenerProducto(id);
	}

	@Override
	public Producto crearProducto(Producto producto) {
		return service.crearProducto(producto);
	}

	@Override
	public List<Producto> consultarProductos() {
		return service.consultarProductos();
	}

}
