package com.trupper.examen_tecnico.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.trupper.examen_tecnico.model.Orden;
import com.trupper.examen_tecnico.service.IOrdenesCompraService;

@RestController
public class OrdenesCompraController implements IOrdenesCompraController {
	
	@Autowired
	private IOrdenesCompraService service;

	@Override
	public int crearOrden(Orden orden) {
		service.crearOrden(orden);
		return orden.getId();
	}

	@Override
	public Orden obtenerOrden(int id) {
		return service.consultarOrdenById(id);
	}

}
