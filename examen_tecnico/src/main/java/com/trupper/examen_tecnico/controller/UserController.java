package com.trupper.examen_tecnico.controller;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.RestController;

import com.trupper.examen_tecnico.model.User;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@RestController
public class UserController implements IUserController {

	@Override
	public User autheticate(String usuario, String password) {
		
		String token = getJWT(usuario);
		
		User user = new User();
		
		user.setId(usuario);
		user.setToken(token);
		
		return user;
	}

	private String getJWT(String usuario) {

		String secretKey = "claveSecreta";

		List<GrantedAuthority> grantedAuthorities = AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER");

		String jwt = Jwts.builder().setId("trupperJWT").setSubject(usuario)
				.claim("authorities",
						grantedAuthorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 600000))
				.signWith(SignatureAlgorithm.HS512, secretKey.getBytes()).compact();

		return "Bearer " + jwt;
	}

}
