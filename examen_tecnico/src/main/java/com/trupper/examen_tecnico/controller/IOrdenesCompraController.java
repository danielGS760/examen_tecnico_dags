package com.trupper.examen_tecnico.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.trupper.examen_tecnico.model.Orden;

@RestController
@RequestMapping(path = "/ordenes")
public interface IOrdenesCompraController {

	@PostMapping("/")
	public int crearOrden(@RequestBody Orden orden);

	@GetMapping("/{id}")
	public Orden obtenerOrden(@PathVariable int id);

}
