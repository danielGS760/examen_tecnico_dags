package com.trupper.examen_tecnico.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.trupper.examen_tecnico.model.Producto;

@RestController
@RequestMapping(path = "/products")
public interface IProductosController {

	@PutMapping("/{id}")
	public Producto actualizarProducto(@RequestBody Producto producto, @PathVariable int id);
	
	@GetMapping("/{id}")
	public Producto obtenerProducto(@PathVariable int id);

	@PostMapping("/")
	public Producto crearProducto(@RequestBody Producto producto);
	
	@GetMapping("/")
	public List<Producto> consultarProductos();
}
