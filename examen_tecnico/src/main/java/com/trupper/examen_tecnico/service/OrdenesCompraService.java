package com.trupper.examen_tecnico.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trupper.examen_tecnico.model.Orden;
import com.trupper.examen_tecnico.model.Producto;
import com.trupper.examen_tecnico.repository.IOrdenesCompraRepository;
import com.trupper.examen_tecnico.repository.IProductosRepository;

@Service
public class OrdenesCompraService implements IOrdenesCompraService {

	@Autowired
	private IOrdenesCompraRepository repository;

	@Autowired
	private IProductosRepository productoRepository;

	@Override
	public void crearOrden(Orden orden) {
		repository.save(orden);
	}

	@Override
	public Orden consultarOrdenById(int id) {
		return repository.findById(id).get();
	}

	@Override
	public int actualizarProducto(Producto producto) {
		Producto productoActualizado = productoRepository.findById(producto.getId()).get();
		productoActualizado.setPrecio(producto.getPrecio());		
		productoRepository.save(productoActualizado);
		return productoActualizado.getId();
	}

}
