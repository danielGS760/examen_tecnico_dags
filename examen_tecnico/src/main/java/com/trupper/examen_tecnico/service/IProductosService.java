package com.trupper.examen_tecnico.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.trupper.examen_tecnico.model.Producto;

@Service
public interface IProductosService {

	public Producto actualizarPrecioProducto(float precio, int id);

	public Producto obtenerProducto(int id);

	public Producto crearProducto(Producto producto);

	public List<Producto> consultarProductos();
}
