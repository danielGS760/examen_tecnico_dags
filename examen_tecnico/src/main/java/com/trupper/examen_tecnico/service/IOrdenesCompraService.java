package com.trupper.examen_tecnico.service;

import org.springframework.stereotype.Service;

import com.trupper.examen_tecnico.model.Orden;
import com.trupper.examen_tecnico.model.Producto;

@Service
public interface IOrdenesCompraService {

	public void crearOrden(Orden orden);
	
	public Orden consultarOrdenById(int id);
		
	public int actualizarProducto(Producto producto);
}
