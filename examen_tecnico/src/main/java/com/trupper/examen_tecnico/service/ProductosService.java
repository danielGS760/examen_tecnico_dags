package com.trupper.examen_tecnico.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trupper.examen_tecnico.model.Producto;
import com.trupper.examen_tecnico.repository.IProductosRepository;

@Service
public class ProductosService implements IProductosService {

	@Autowired
	private IProductosRepository repository;

	@Override
	public Producto actualizarPrecioProducto(float precio, int id) {

		Producto producto = repository.findById(id).get();

		if (producto != null) {
			producto.setPrecio(precio);
			return repository.save(producto);
		} else {
			return null;
		}
	}

	@Override
	public Producto obtenerProducto(int id) {
		return repository.findById(id).get();
	}

	@Override
	public Producto crearProducto(Producto producto) {
		return repository.save(producto);
	}

	@Override
	public List<Producto> consultarProductos() {
		List<Producto> productos = new ArrayList<>();

		repository.findAll().forEach(producto -> {
			productos.add(producto);
		});

		return productos;
	}

}
