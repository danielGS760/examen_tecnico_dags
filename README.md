# examen_tecnico_dags

Se cuenta con implementacion jwt, por lo que para que puedan ser ejecutados los servicios debe realizarse lo siguiente:

1. Ejecutar servicio de login: 

    - Http Method: POST 
    - Path: /users/login
    - Request Parameters:
        - usuario
        - pass

de la respuesta de este servicio de toma el token y se envia en los demas endpoints a traves del header Authorization.

NOTA: No se realiza validación contra Base de datos.

2. Los servicios disponibles son:

- Para crear orden de compra: 

    - Http Method: POST
    - Path: /ordenes/
    - Body:
        {
            "id": 1,
            "fecha": "2022-07-25",
            "total": 12.34,
            "sucursal": {
                "id": 1,
                "nombre": "CDMX"
            },
            "productos": [
                {
                    "codigo": "18156",
                    "descripcion": "Esmeriladora angular",
                    "precio": 625.00
                },
                {
                    "codigo": "17193",
                    "descripcion": "Pala redonda",
                    "precio": 100.50
                }
            ]
        }

    - Response: id orden

- Para consulta orden de compra: 
    - Http Method:GET
    - Path: /ordenes/{id}
    - donde: id = id orden a consultar

- Para actualizacion de precio de producto:

    - Http Method:PUT
    - Path: /products/{id}
    - donde: id = id producto a consultar
    - Request Body:
        {
            "codigo": "17193",
            "descripcion": "Pala redonda",
            "precio": 99.99
        }

- Crea producto:

    - Http Method:POST
    - Path: /products/
    - Request Body:
        {
            "codigo": "17193",
            "descripcion": "Pala redonda",
            "precio": 99.99
        }

- Consulta productos registrados:

    - Http Method:GET
    - Path: /products/

